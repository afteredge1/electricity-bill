**Electricity Bill**

Write an interface named electricityBill that will calculate and print bills for the city
power company. The rates vary depending on whether the use is residential, commercial,
or industrial. A code of R means residential use, a code of C means commercial use, and
a code of I means industrial use.

The rates are computed as follows:<br>
R: RM6.00 plus RM0.052 per kwh used<br>
C: RM60.00 for the first 1000 kwh and RM0.045 for each additional kwh<br>
I: Rate varies depending on time of usage:<br>
&nbsp;&nbsp;&nbsp;&nbsp;1. Peak hours: RM76.00 for first 1000 kwh<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;and RM0.065 for each additional kwh<br>
&nbsp;&nbsp;&nbsp;&nbsp;2. Off peak hours: RM40.00 for first 1000 kwh<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;and RM0.028 for each additional kwh.<br>
<br>
_Note_ that if the code is I, an additional input to determine whether it is peak or off peak is
required.<br>

The interface should display the amount due from the user, rounded to 2 decimal points.
