<?php
namespace ElectricityBill;

class BillFactory
{
    public function MakeResidential($units)
    {
        return new ResidentialBill($units);
    }

    public function MakeCommercial($units)
    {
        return new CommercialBill($units);
    }

    public function MakeIndustrial($units, $type)
    {
        return new Industrialbill($units, $type);
    }
}
