<?php
namespace ElectricityBill;

class Bill
{
    public function Main()
    {
        $units = 50;
        $type = 'P'; // P for peak hours & O for off peak hours

        $bill = new BillFactory();

        $r = $bill->MakeResidential($units);
        echo "<br>Residential bill Amount: RM".$r->BillAmount();

        $c = $bill->MakeCommercial($units);
        echo "<br>Commercial bill Amount: RM".$c->BillAmount();

        $i = $bill->MakeIndustrial($units, $type);
        echo "<br>Industrial bill Amount: RM".$i->BillAmount();
    }
}
