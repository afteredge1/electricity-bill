<?php
namespace ElectricityBill;

// import contract
use ElectricityBill\contracts\ResidentialBillContract;

class ResidentialBill implements ResidentialBillContract
{
    public function __construct($units)
    {
        $this->units = $units;
        $this->prefix = 6;
        $this->bill_charges = '0.00';
        $this->price = '0.052';
    }

    public function BillAmount()
    {
        $this->bill_charges = ($this->units * $this->price) + $this->prefix;
        return number_format((float)$this->bill_charges, 2, '.', '');
    }
}
