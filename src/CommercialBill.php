<?php
namespace ElectricityBill;

// import contract
use ElectricityBill\contracts\CommercialBillContract;

class CommercialBill implements CommercialBillContract
{
    public function __construct($units)
    {
        $this->units = $units;
        $this->prefix = 60;
        $this->bill_charges = '0.00';
        $this->price = '0.045';
        $this->fix_units = 1000;
    }

    public function BillAmount()
    {
        if ($this->units > $this->fix_units) {
            $charge_able = $this->units - $this->fix_units;
            $this->bill_charges = ($charge_able * $this->price) + $this->prefix;
        } else {
            $this->bill_charges = $this->prefix;
        }

        return number_format((float)$this->bill_charges, 2, '.', '');
    }
}
