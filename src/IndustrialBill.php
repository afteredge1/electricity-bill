<?php
namespace ElectricityBill;

// import contract
use ElectricityBill\contracts\IndustrialBillContract;

class IndustrialBill implements IndustrialBillContract
{
    public function __construct($units, $type)
    {
        $this->units = $units;
        $this->bill_type = $type;
        $this->prefix_peak_hour = 76;
        $this->prefix_off_peak_hour = 40;
        $this->bill_charges = '0.00';
        $this->price_peak_hour = '0.065';
        $this->price_off_peak_hour = '0.028';
        $this->fix_units = 1000;
    }

    public function BillAmount()
    {
        if ($this->bill_type == 'P') {
            return number_format((float)$this->_billAmountPeakHours(), 2, '.', '');
        } else if ($this->bill_type == 'O') {
            return number_format((float)$this->_billAmountOffPeakHours(), 2, '.', '');
        } else {
            return "Invalid hours type";
        }
    }

    /**
     * private functions
     */

    private function _billAmountPeakHours()
    {
        if ($this->units > $this->fix_units) {
            $charge_able = $this->units - $this->fix_units;
            $this->bill_charges = ($charge_able * $this->price_peak_hour) + $this->prefix_peak_hour;
        } else {
            $this->bill_charges = $this->prefix_peak_hour;
        }

        return $this->bill_charges;
    }

    private function _billAmountOffPeakHours()
    {
        if ($this->units > $this->fix_units) {
            $charge_able = $this->units - $this->fix_units;
            $this->bill_charges = ($charge_able * $this->price_off_peak_hour) + $this->prefix_off_peak_hour;
        } else {
            $this->bill_charges = $this->prefix_off_peak_hour;
        }

        return $this->bill_charges;
    }
}
